package com.guiyunweb.o2o.service.impl;

import com.guiyunweb.o2o.BastTest;
import com.guiyunweb.o2o.entity.Area;
import com.guiyunweb.o2o.service.AreaService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public class AreaServiceTest extends BastTest {

    @Autowired
    private AreaService areaService;

    @Test
    public void queryArea() {
        List<Area> areaList  =areaService.getAreaList();

        for (Area a: areaList) {
            System.out.println(a.toString());
        }
    }

}