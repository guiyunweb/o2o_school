package com.guiyunweb.o2o.dao;

import com.guiyunweb.o2o.BastTest;
import com.guiyunweb.o2o.entity.Area;
import com.guiyunweb.o2o.entity.PersonInfo;
import com.guiyunweb.o2o.entity.Shop;
import com.guiyunweb.o2o.entity.ShopCategory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

public class ShopDaoTest extends BastTest {

    @Autowired
    private ShopDao shopDao;

    @Test
    public void insertShop() {
        Shop shop = new Shop();
        PersonInfo personInfo = new PersonInfo();
        Area area = new Area();
        ShopCategory category = new ShopCategory();
        personInfo.setUserId(10L);

        area.setareaId(3);
        category.setShopCategoryID(10L);
        shop.setPersonInfo(personInfo);
        shop.setArea(area);
        shop.setShopCategory(category);
        shop.setShopName("测试的店铺");
        shop.setShopDesc("这是一个描述");
        shop.setShopAddr("这是地点");
        shop.setPhone("10010");
        shop.setShopImg("https://www.baidu.com/img/baidu_85beaf5496f291521eb75ba38eacbd87.svg");
        shop.setcTime(new Date());
        shop.setEnableStatus(1);
        shop.setAdvice("审核中");

        int effectedMum  = shopDao.insertShop(shop);
        System.out.println(effectedMum);
    }

    @Test
    public void updateShop() {
        Shop shop = new Shop();
        shop.setShopId(28L);

        shop.setShopDesc("这是一个描述");
        shop.setShopAddr("这是地点");
        shop.setPhone("10010");
        shop.setShopImg("https://www.baidu.com/img/baidu_85beaf5496f291521eb75ba38eacbd87.svg");
        shop.setcTime(new Date());
        shop.setEnableStatus(1);
        shop.setAdvice("审核中");
        shop.setuTime(new Date());

        int effectedMum  = shopDao.updateShop(shop);
        System.out.println(effectedMum);
    }
}