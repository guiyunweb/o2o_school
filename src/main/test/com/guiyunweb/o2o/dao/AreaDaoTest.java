package com.guiyunweb.o2o.dao;

import com.guiyunweb.o2o.BastTest;
import com.guiyunweb.o2o.entity.Area;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AreaDaoTest extends BastTest {

    @Autowired
    private AreaDao areaDao;

    @Test
    public void queryArea() {
        List<Area> areaList  =areaDao.queryArea();

        System.out.println("++++++++++++++++");
        System.out.println(areaList.get(0).toString());
        System.out.println("++++++++++++++++");
    }
}