package com.guiyunweb.o2o.entity;

import java.util.Date;

/**
 * 区域
 */
public class Area {
    private Integer areaId;     //id

    private String areaName;    //名称
    private Integer priority;   //权重
    private Date cTime;         //创建时间
    private Date uTime;         //更新  时间

    public Integer getareaId() {
        return areaId;
    }

    public void setareaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getareaName() {
        return areaName;
    }

    public void setareaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Date getcTime() {
        return cTime;
    }

    public void setcTime(Date cTime) {
        this.cTime = cTime;
    }

    public Date getuTime() {
        return uTime;
    }

    public void setuTime(Date uTime) {
        this.uTime = uTime;
    }

    @Override
    public String toString() {
        return "Area{" +
                "areaId=" + areaId +
                ", areaName='" + areaName + '\'' +
                ", priority=" + priority +
                ", cTime=" + cTime +
                ", uTime=" + uTime +
                '}';
    }
}
