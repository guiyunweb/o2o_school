package com.guiyunweb.o2o.entity;

import java.util.Date;

public class WechatAuth {
    private Long wechatAuthID;
    private Long openId;
    private Date cTime;
    private PersonInfo personInfo;

    public Long getWechatAuthID() {
        return wechatAuthID;
    }

    public void setWechatAuthID(Long wechatAuthID) {
        this.wechatAuthID = wechatAuthID;
    }

    public Long getOpenId() {
        return openId;
    }

    public void setOpenId(Long openId) {
        this.openId = openId;
    }

    public Date getcTime() {
        return cTime;
    }

    public void setcTime(Date cTime) {
        this.cTime = cTime;
    }

    public PersonInfo getPersonInfo() {
        return personInfo;
    }

    public void setPersonInfo(PersonInfo personInfo) {
        this.personInfo = personInfo;
    }
}
